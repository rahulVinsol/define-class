class SomeClass {
	let uneditableProp: String
	var editableProp: String
	private(set) var privateSetterProp: String
	var computedProp: String {
		"\(uneditableProp)\t\(editableProp)"
	}
	
	init() {
		uneditableProp = "I am uneditable"
		editableProp = "And I... am.... the editable prop"
		privateSetterProp = "Private Setter Prop"
	}
	
	init(_ uneditableProp: String, _ editableProp: String, _ privateSetterProp: String){
		self.uneditableProp = uneditableProp
		self.editableProp = editableProp
		self.privateSetterProp = privateSetterProp
	}
	
	func toString() -> String {
		"""
			uneditableProp    -> \(uneditableProp)
			editableProp      -> \(editableProp)
			privateSetterProp -> \(privateSetterProp) 
			computedProp  -> \(computedProp)
		"""
	}
}

let instance = SomeClass()
print(instance.computedProp)
print(instance.toString())
